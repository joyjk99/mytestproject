<?php

use Illuminate\Support\Facades\Route;


Route::get('student/home', [App\Http\Controllers\StudentController::class, 'studentHome'])->name('student.home')->middleware('is_student');


Route::group(['prefix'=>'student', 'namespace' => 'App\Http\Controllers'],function(){

    Route::get('home', 'StudentController@studentHome')->name('student.home')->middleware('is_student');
});
