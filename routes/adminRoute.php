<?php

use Illuminate\Support\Facades\Route;

Route::get('admin/home', [App\Http\Controllers\AdminController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');


Route::group(['prefix' => 'admin', 'namespace' =>'App\Http\Controllers'],function(){

    Route::get('home', 'AdminController@adminHome')->name('admin.home')->middleware('is_admin');
});